import discord
from twilio.rest import TwilioRestClient
from discord.ext import commands
import logging
import urllib.request
import requests
import json
import asyncio
import re
import sqlite3
import duckduckpy
import collections
import os

#connects to db which contains item name to item id mappings
#db = sqlite3.connect('itemdb')
# creates cursor handle so we can pull records and such
#cursor = db.cursor()


logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)


description = 'carbon  bot'

author_dict = {'postfix' : '199624730230128640'}
# opus is required for voice functionality so we must load it if it is not loaded
if not discord.opus.is_loaded():
    discord.opus.load_opus('opus')


class Sms:

    """Allows sending SMS and MMS messages. This is a restricted command group limited to postfix only"""
    def __init__(self, account_sid, auth_token, bot):
        self.bot = bot
        self.account_sid = account_sid
        self.auth_token = auth_token
        # creates a handle for twilio rest client, using account_sid and auth_token for authentication
        self.client = TwilioRestClient(self.account_sid, self.auth_token)
        # this is the number that all messages will be coming from
        self.from_num = '+12897688093'
        self.numbers = []
        with open('numbers.txt', 'r') as fh:
            for line in fh.readlines():
                self.numbers.append(line.strip('\n'))

    def send_text(self, to_num, msg):
        # whenever using a \ in regex, always use the r'..' to indicate rawstring
        # checks to see if the destination number provided is formatted correctly
        if not re.search(r'\+[0-9]*', str(to_num)):
            print("The phone number does not have a '+' at the beginning, please re-enter the number")
            to_num = input('Destination number: ')
    # constructs the message,  creates, and sends it to the 'to' number
    # message handle is created in case we need to manipulate message  or retrieve information later on
        message = self.client.messages.create(to=to_num,
                                            from_=self.from_num,
                                            body=msg)
        return True

    def send_picture(self, to_num, msg, pic_url):
        # this is used to send an mms
        if not re.search(r'\+[0-9]*', to_num):
            print("The phone number does not have a '+' at the beginning, please re-enter the number")
            to_num = input('Destination number: ')

        messages = self.client.message.create(body=msg,
                                         to=to_num,
                                         from_=self.from_num,
                                         media_url=pic_url)

    @commands.command(pass_context=True, description='Send SMS message')
    async def send_sms(self, ctx, number : str, *, message : str):
        """Sends an SMS message. Access limited to postfix"""
        # if str(ctx.message.author) != 'sirvapeslot(postfix)#2265':
        if str(ctx.message.author.id) not in ['199624730230128640']:
            await bot.say('You do not have permissions to perform this command')
        else:
            msg = ctx.message
            if self.send_text(number, message):
                await bot.send_message(msg.channel, 'Message sent successfully')
            else:
                await bot.send_message(msg.channel, 'Message not sent')

    @send_sms.error
    async def _send_sms(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            await self.bot.say('Missing Arguments:\n$send_sms <to-num> "<msg>"')

    @commands.command(pass_context=True, description='Add number to valid numbers database')
    async def add_number(self, ctx, number : str):
        """Adds a number to the valid numbers list. Access limited to postfix"""
        """Sends an SMS message"""
        if str(ctx.message.author.id) not in ['199624730230128640']:
            await bot.say('You do not have permissions to perform this command')
        else:
            if not re.search(r'\+[0-9]*', str(number)):
                await bot.say('The number must include a +1 at the beginning please add it')
            else:
                await bot.say('Number added.\nThis requires postfix to take an additional action.\nPlease contact him')
                self.numbers.append(number)
                with open('numbers.txt', 'a') as fh:
                    fh.write('%s\n' % number)
    @add_number.error
    async def _add_number(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            await bot.say('Missing arguments\nUsage: $add_number +1<number>')


class Item:

    def __init__(self, bot):
        self.bot = bot
        #self.cursor = cursor
        self.stat_ids = {
            1: 'Health',
            2: 'Mana',
            3: 'Agility',
            4: 'Strength',
            5: 'Intellect',
            6: 'Spirit',
            7: 'Stamina',
            32: 'Crit Strike',
            36: 'Haste',
            40: 'Versatility',
            49: 'Mastery',
        }

    @commands.command(pass_context=True, description='look up statistics for gear items')
    async def item_lookup(self, ctx, item_id : str, req_lvl = 'test'):
        """Look up statistics for gear items in WoW"""
        if req_lvl != 'test':
            print('test')
            print(item_id)
            db = sqlite3.connect('itemdb')
            print('test2')
            cursor = db.cursor()
            cursor.execute('''select itemid from items where name = ? and reqlvl = ? ''', (item_id, req_lvl,))
            lst = cursor.fetchall()
            print(lst)
            item_id = lst[0][0]
            db.close()
        url = 'http://www.wowdb.com/api/item/%s' % item_id
        json_data = json.loads(requests.get(url).content.decode('utf-8')[1:-1])
        name = json_data['Name']
        item_lvl = json_data['Level']
        stats = json_data['Stats']
        #await bot.say('Name: %s\nItem Level: %s' % (name, item_lvl))
        embed = discord.Embed()
        embed.title = str(name)
        img_url = json_data['Icon']
        embed.add_field(name='Item Level', value=item_lvl)
        try:
            embed.set_thumbnail(url='https://wow.zamimg.com/images/wow/icons/large/%s.jpg' % img_url)
        except Exception as e:
            print(e)
        for i in range(0, len(stats)):
            embed.add_field(name=str(self.stat_ids[stats[i]['StatID']]), value =str(stats[i]['Quantity']))
        await self.bot.say(embed=embed)

    @item_lookup.error
    async def _item_lookup(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            msg = ("\n"
                   "Missing arguments.\n"
                   "Usage:\t\t$item_lookup <item-id> <req-lvl>\n"
                   "<item-id> can be and item name or item id. If supplying item name req-lvl must be filled out\n")
            print(type(error))
            await self.bot.say(msg)
        elif type(error) is commands.CommandInvokeError:
            print(error)
            await self.bot.say('Incorrect <item-name> used')


class Misc:

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, description='Spam a users name to get their attention')
    async def spam(self, ctx, member: discord.Member, amount: int, msg: str):
        """spam a users name to get their attention"""
        if str(ctx.message.author.id) not in ['199624730230128640']:
            await self.bot.say('You do not have permissions to use this command')
        else:
            if amount > 10:
                amount = 10
            try:
                for i in range(0, amount):
                    await self.bot.say('%s %s' % (member.mention, msg))
            except Exception as e:
                print(e)
    @spam.error
    async def _spam(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            await self.bot.say('Missing arguments.\nUsage: $spam <user> <num_msg> "<msg>"')


    @commands.command(pass_context=True, description='bot cleanup')
    async def bot_cleanup(self, ctx, num_msgs : int):
        """cleans up messages generated by the cot"""
        channel = ctx.message.channel
        async for entry in self.bot.logs_from(channel, limit=num_msgs):
            if entry.author == self.bot.user:
                await self.bot.delete_message(entry)
    @bot_cleanup.error
    async def _bot_cleanup(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            await self.bot.say('Missing arguments.\nUsage: $bot_cleanup <num-msgs>\n<num-msgs> is amount of messages')

    @commands.command(pass_context=True, description='duck duck go search')
    async def ddg(self, ctx, *, search : str):
        """Returns the most related topic from duck duck go"""
        response = duckduckpy.query(str(search), container=u'dict')
        related_topics = response['related_topics']
        brief_summary = related_topics[0]['text']
        more_info = related_topics[0]['first_url']
        await self.bot.say('%s\nMore information: %s' % (brief_summary, more_info))
    @ddg.error
    async def _ddg(self, error, ctx):
        if type(error) is IndexError:
            await self.bot.say('Invalid search term')





class Notes:

    def __init__(self, bot):
        self.bot = bot
        self.db_name = 'notedb'

    @commands.group(pass_context=True, description='note command set')
    async def notes(self, ctx):
        """ used to retrieve, and display notes from the database"""
        if ctx.invoked_subcommand is None:
            await self.bot.say('No subcommand provided\nUse <$notes help> for more information')

    @notes.command()
    async def help(self):
        msg = 'This command set is used for interaction with the note functionality of the bot\n'
        msg += 'Available subcommands:\n\thelp\n\tread\n\twrite\n\tlist\n'
        msg += 'Please note that you are unable to view notes other than ones owned by you'
        await self.bot.say(msg)

class VoiceEntry:
    def __init__(self, message, player):
        self.requester = message.author
        self.channel = message.channel
        self.player = player

    def __str__(self):
        fmt = '{0.title} uploaded by {0.uploader} and requested by {1.display_name}'
        duration = self.player.duration
        if duration:
            fmt = fmt + ' [length: {0[0]}m {0[1]}s]'.format(divmod(duration, 60))
        return fmt.format(self.player, self.requester)

class VoiceState:
    def __init__(self, bot):
        self.current = None
        self.voice = None
        self.bot = bot
        self.play_next_song = asyncio.Event()
        self.songs = asyncio.Queue()
        self.skip_votes = set() # a set of user_ids that voted
        self.audio_player = self.bot.loop.create_task(self.audio_player_task())

    def is_playing(self):
        if self.voice is None or self.current is None:
            return False

        player = self.current.player
        return not player.is_done()

    @property
    def player(self):
        return self.current.player

    def skip(self):
        self.skip_votes.clear()
        if self.is_playing():
            self.player.stop()

    def toggle_next(self):
        self.bot.loop.call_soon_threadsafe(self.play_next_song.set)

    async def audio_player_task(self):
        while True:
            self.play_next_song.clear()
            self.current = await self.songs.get()
            await self.bot.send_message(self.current.channel, 'Now playing ' + str(self.current))
            self.current.player.start()
            await self.play_next_song.wait()


class Music:

    """Voice related commands.
    Works in multiple servers at once.
    """
    def __init__(self, bot):
        self.bot = bot
        self.voice_states = {}
        self.song_list = []

    def get_voice_state(self, server):
        state = self.voice_states.get(server.id)
        if state is None:
            state = VoiceState(self.bot)
            self.voice_states[server.id] = state

        return state

    async def create_voice_client(self, channel):
        voice = await self.bot.join_voice_channel(channel)
        state = self.get_voice_state(channel.server)
        state.voice = voice

    def __unload(self):
        for state in self.voice_states.values():
            try:
                state.audio_player.cancel()
                if state.voice:
                    self.bot.loop.create_task(state.voice.disconnect())
            except:
                pass

    @commands.command(pass_context=True, no_pm=True)
    async def join(self, ctx, *, channel : discord.Channel):
        """Joins a voice channel."""
        try:
            await self.create_voice_client(channel)
        except discord.ClientException:
            await self.bot.say('Already in a voice channel...')
        except discord.InvalidArgument:
            await self.bot.say('This is not a voice channel...')
        else:
            await self.bot.say('Ready to play audio in ' + channel.name)

    @commands.command(pass_context=True, no_pm=True)
    async def summon(self, ctx):
        """Summons the bot to join your voice channel."""
        summoned_channel = ctx.message.author.voice_channel
        if summoned_channel is None:
            await self.bot.say('You are not in a voice channel.')
            return False

        state = self.get_voice_state(ctx.message.server)
        if state.voice is None:
            state.voice = await self.bot.join_voice_channel(summoned_channel)
        else:
            await state.voice.move_to(summoned_channel)

        return True

    @commands.command(pass_context=True, no_pm=True)
    async def play(self, ctx, *, song : str):
        """Plays a song.
        If there is a song currently in the queue, then it is
        queued until the next song is done playing.
        This command automatically searches as well from YouTube.
        The list of supported sites can be found here:
        https://rg3.github.io/youtube-dl/supportedsites.html
        """
        state = self.get_voice_state(ctx.message.server)
        opts = {
            'default_search': 'auto',
            'quiet': True,
            'format': 'bestaudio/best',
        }

        if state.voice is None:
            success = await ctx.invoke(self.summon)
            if not success:
                return

        try:
            player = await state.voice.create_ytdl_player(song, ytdl_options=opts, after=state.toggle_next)
            player.volume = 0.15
        except Exception as e:
            fmt = 'An error occurred while processing this request: ```py\n{}: {}\n```'
            await self.bot.send_message(ctx.message.channel, fmt.format(type(e).__name__, e))
        else:
            player.volume = 0.15
            entry = VoiceEntry(ctx.message, player)
            await self.bot.say('Enqueued ' + str(entry))
            await state.songs.put(entry)
    @play.error
    async def _play(self, error, ctx):
        if type(error) is commands.CommandOnCooldown:
            await bot.say('Command is rate limited.\nPlease wait 60 seconds and try again')


    @commands.command(pass_context=True, no_pm=True)
    async def get_que(self, ctx):
        """ prints a list of all songs in the que """
        # take a look at deque in order to accomplish this
        msg = ctx.message
        state = self.get_voice_state(ctx.message.server)
        player = state.player
        entry = VoiceEntry(ctx.message, player)
        song_list = []
        # by using ._queue we gain access to the internal system which is dequeu
        # so all deque operations can be done by state.songs._queue._________
        # it shouold be known that accessing the internals this way could cause a break
        for song in state.songs._queue:
            song_list.append(str(song))
        msg = "Queued songs:\n"
        for song in song_list:
            msg += "%s\n" % song
        await self.bot.say(msg)

    @commands.command(pass_context=True, no_pm=True)
    async def rm_que(self, ctx, *, song_name : str):
        """ remove a particular song from the que """
        state = self.get_voice_state(ctx.message.server)
        for song in state.songs._queue:
            if re.search(song_name, str(song)):
                await state.songs._queue.remove(song)
    @rm_que.error
    async def _rm_que(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            await bot.say('Inccorect argument.\nUsage: $rm_que <song-name>\nSong name must be capitalized correctly')

    @commands.command(pass_context=True, no_pm=True)
    async def volume(self, ctx, value: int):
        """Sets the volume of the currently playing song."""
        msg = ctx.message
        if str(msg.author) in ['schmoodled#2663', 'sirvapeslot(postfix)#2265']:
            state = self.get_voice_state(ctx.message.server)
            if state.is_playing():
                player = state.player
                player.volume = value / 100
                await self.bot.say('Set the volume to {:.0%}'.format(player.volume))
        else:
            await self.bot.say('You do not have permissions to do that')

    @commands.command(pass_context=True, no_pm=True)
    async def pause(self, ctx):
        """Pauses the currently played song."""
        state = self.get_voice_state(ctx.message.server)
        if state.is_playing():
            player = state.player
            player.pause()

    @commands.command(pass_context=True, no_pm=True)
    async def resume(self, ctx):
        """Resumes the currently played song."""
        state = self.get_voice_state(ctx.message.server)
        if state.is_playing():
            player = state.player
            player.resume()

    @commands.command(pass_context=True, no_pm=True)
    async def stop(self, ctx):
        """Stops playing audio and leaves the voice channel.
        This also clears the queue.
        """
        server = ctx.message.server
        state = self.get_voice_state(server)

        if state.is_playing():
            player = state.player
            player.stop()

        try:
            state.audio_player.cancel()
            del self.voice_states[server.id]
            await state.voice.disconnect()
        except:
            pass

    @commands.command(pass_context=True, no_pm=True)
    async def skip(self, ctx):
        """Vote to skip a song. The song requester can automatically skip.
        3 skip votes are needed for the song to be skipped.
        """

        state = self.get_voice_state(ctx.message.server)
        if not state.is_playing():
            await self.bot.say('Not playing any music right now...')
            return

        voter = ctx.message.author
        if voter == state.current.requester:
            await self.bot.say('Requester requested skipping song...')
            state.skip()
        elif voter.id not in state.skip_votes:
            state.skip_votes.add(voter.id)
            total_votes = len(state.skip_votes)
            if total_votes >= 3:
                await self.bot.say('Skip vote passed, skipping song...')
                state.skip()
            else:
                await self.bot.say('Skip vote added, currently at [{}/3]'.format(total_votes))
        else:
            await self.bot.say('You have already voted to skip this song.')

    @commands.command(pass_context=True, no_pm=True)
    async def playing(self, ctx):
        """Shows info about the currently played song."""

        state = self.get_voice_state(ctx.message.server)
        if state.current is None:
            await self.bot.say('Not playing anything.')
        else:
            skip_count = len(state.skip_votes)
            await self.bot.say('Now playing {} [skips: {}/3]'.format(state.current, skip_count))

class Image:

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, description='bot uploads img from img store to channel')
    async def upload(self, ctx, *, img_name : str):
        msg = ctx.message
        await bot.send_file(msg.channel, img_name)

    @upload.error
    async def upload_error(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            await bot.say('Missing arguments.\nUsage: ?upload <img-name>')

    @commands.command(pass_context=True, description='add image to image datastore for bot')
    async def addimg(self, ctx, img_name : str, *, img_path: str):
        msg = ctx.message
        if str(ctx.message.author.id) not in ['199624730230128640']:
            if urllib.request.urlretrieve(img_path, "images/%s" % img_name):
                urllib.request.urlretrieve(img_path, "images/%s" % img_name)
                await bot.say("Image successfully downloaded")
            else:
                await bot.say("Error downloading image")
        else:
            bot.say('You do not have permissions to use this command')

    @addimg.error
    async def addimg_error(self, error, ctx):
        if type(error) is commands.MissingRequiredArgument:
            await bot.say('Missing arguments.\nUsage: ?addimg <img-name> <img-url>')


class Embed_Test:

    def __init__(self,bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def embed_test(self, ctx):
        embed = discord.Embed()
        embed.title = 'Warlock Pants'
        embed.add_field(name='Stamina', value='saaa')
        embed.add_field(name='Intellect', value='sdas')
        embed.set_thumbnail(url='https://wow.zamimg.com/images/wow/icons/large/inv_cloth_raidpriest_q_01_shoulder.jpg')
        await self.bot.say(embed=embed)

bot = commands.Bot(command_prefix=commands.when_mentioned_or('$'), description=description)
bot.add_cog(Image(bot))
bot.add_cog(Item(bot))
bot.add_cog(Music(bot))
bot.add_cog(Sms(account_sid, auth_token, bot))
#bot.add_cog(Embed_Test(bot))
bot.add_cog(Misc(bot))
bot.add_cog(Notes(bot))
bot.run(token)
